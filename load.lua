trucktran = 2
truckcounter = 0



cursorx=300
cursory=600

fires=0

moonshader = love.graphics.newShader[[
    extern number moonposx;
    extern number moonposy;
    vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords ){
      vec4 pixel = Texel(texture, texture_coords );//This is the current pixel color
      
      number d = sqrt(pow(moonposx-screen_coords.x,2)+pow(moonposy-screen_coords.y,2));
      
      if(d < 800){
        pixel.r = pixel.r+(110/(d*d));
        pixel.g = pixel.g+(110/(d*d));
        pixel.b = pixel.b+(110/(d*d));
      }
      

      
      return pixel * color;
    }
  ]]

titlescreen=true
gameover=false

tstran=0
gotran=0



timer = 75
gamestart=false
love.window.setMode(1920/1,1080/1,{vsync=false,fullscreen=true})
--love.window.setMode(1920,1080)
transky=0
--Texture generation
istransition=false
skydat = love.image.newImageData(1920,1080)

love.graphics.setDefaultFilter("nearest")

moon = love.graphics.newImage("assets/moon.png")
cursor = love.graphics.newImage("assets/cursor.png")
wheel=love.graphics.newImage("assets/wheel.png")

trucksound = love.audio.newSource("assets/truck.wav","stream")

function skymap(x,y,r,g,b,a)
  

  
  v = (love.math.noise(x/25500,y/25500)+love.math.noise(x/25500+1000,y/25500+1000)+love.math.noise(x/255+2000,y/255+2000)+love.math.noise(x/255+3000,y/255+3000)+love.math.noise(x/255+4000,y/255+4000))/5
  
  r=v*140/6
  g=v*66/6
  b=v*244/6
  
  --love.math.setRandomSeed()
  if love.math.random(1,115) == 5 then
    --love.math.setRandomSeed(os.time()*.1)
    --r = love.math.random(0,255)
    --love.math.setRandomSeed(os.time()*.2)
    g = love.math.random(0,255)
   --love.math.setRandomSeed(os.time()*.3)
    b = love.math.random(0,255)
    
  end
  
  
  return r,g,b,255
  
  
end
city=love.graphics.newImage("assets/cityskyline.png")
skydat:mapPixel(skymap)

sky=love.graphics.newImage(skydat)


go = love.graphics.newImage("assets/guits.png")
ts = love.graphics.newImage("assets/guigo.png")

hits = {}

water = {}
for x=1,100 do
  love.math.setRandomSeed(os.time()+x/3)
  water[x]={}
  water[x]["r"]=(love.math.random()-.5)/15
end



fcounter = 0
dtg=0
cannonposx=0
cannonposy=0

scalene = 0

da={}
da["x"]=0
da["y"]=0

level = 2

music = love.audio.newSource("assets/music.mp3")
music:isLooping(true)
music:setVolume(.05)


road = love.graphics.newImage("assets/road.png")

scale = .5

jeff = love.graphics.newImage("assets/white_dude.jpg")

truck = love.graphics.newImage("assets/firetruck.png")




didfixhit=false
fixhit={}
fixhit["c"]=1
fixhit["r"]=1

wind = love.graphics.newImage("assets/wind.png")

tnumber=0
tcounter=0

rand = 1
fires=0
fire = {}

fire[1] = love.graphics.newImage("assets/fire1.png")
fire[2] = love.graphics.newImage("assets/fire2.png")
fire[3] = love.graphics.newImage("assets/fire3.png")
fire[4] = love.graphics.newImage("assets/fire4.png")

foundleft = love.graphics.newImage("assets/foundleft.png")
foundright = love.graphics.newImage("assets/foundright.png")
foundmid = love.graphics.newImage("assets/foundmid.png")

roofleft = love.graphics.newImage("assets/roofleft.png")
roofright = love.graphics.newImage("assets/roofright.png")
roofmid = love.graphics.newImage("assets/roofmid.png")

cannon = love.graphics.newImage("assets/can.png")
