--[[

Opening of main thread; code execution begins here.

Authored for TCS Robotics team by Brodie Alexander.

]]--

love.filesystem.load("load.lua")()


function genbuilding(l)
  w=math.floor((l)*.5+3)
  
  building = {}
  
  lparams = {}
  
  lparams["firespread"]=4-.1*level
  lparams["width"]=w
  
  for x=1,w do
    building[x]={}
    for y=1,w do
      building[x][y]=0
    end
  end
  
  for x=1,3 do
    love.math.setRandomSeed(os.time()+x+2)
    ra=love.math.random(1,w)
    love.math.setRandomSeed(os.time()+x+4)
    building[ra][love.math.random(1,w)] = 2
  end
  
  lparams["building"] = building
  
  return lparams
  
  
  
end

lparams = {}
bu={}
bu[0]={}
bu[0]["building"]={}
bu[0]["width"]=3
bu[1]=genbuilding(1)
bu[2]=genbuilding(2)
bu[3]=genbuilding(3)
bu[4]=genbuilding(4)
bu[5]=genbuilding(5)
bu[6]=genbuilding(6)
bu[7]=genbuilding(7)
bu[8]=genbuilding(8)
bu[9]=genbuilding(9)
bu[10]=genbuilding(10)
bu[11]=genbuilding(11)
lparams=bu[1]
nexbuil=bu[2]

transx=0

joysticks = love.joystick.getJoysticks()
wheelrot=0
function lerp(a,b,t) return a * (1-t) + b * t end
function updatewater(dt)
  --[[if istransition then
    lerp((city:getWidth()*(love.graphics.getWidth()/city:getWidth())*(1)),0)
  end
end]]
end
function love.draw()
  
  if level==11 then
    sky=jeff
  end
  
  moonshader:send("moonposy",love.graphics.getHeight()/8+8*2)
  moonshader:send("moonposx",love.graphics.getWidth()/8+8*2)
  love.graphics.setShader(moonshader)
  love.graphics.draw(sky,0,0,0,love.graphics.getWidth()/sky:getWidth(),love.graphics.getHeight()/sky:getHeight())
  love.graphics.setShader()
  love.graphics.draw(moon,love.graphics.getWidth()/8,love.graphics.getHeight()/8,0,4,4)
  
  for x=1,69 do
  
    love.graphics.draw(city,(city:getWidth()*(love.graphics.getWidth()/city:getWidth())*(x-1))+transky/10,0,0,love.graphics.getWidth()/city:getWidth(),love.graphics.getHeight()/city:getHeight())
  
  end
  
  --love.graphics.scale(scale)
  love.graphics.scale(scale)
  love.graphics.translate(transx,love.graphics.getHeight()*((1/scale)-1))
  
  scalene = love.graphics.getWidth()/road:getWidth()
  
  for x=0,69 do
    love.graphics.draw(road,road:getWidth()*scalene/3*x,love.graphics.getHeight()-road:getHeight()*(scalene/3),0,scalene/3,scalene/3)
  end
  
  --love.graphics.draw(road,0,love.graphics.getHeight()-road:getHeight()*(scalene/3),0,scalene/3,scalene/3)
 -- love.graphics.draw(road,road:getWidth()*scalene/3,love.graphics.getHeight()-road:getHeight()*(scalene/3),0,scalene/3,scalene/3)
  
  
  
  truckposx = love.graphics.getWidth()/18
  truckposy = love.graphics.getHeight()-love.graphics.getHeight()/3.5
  
  
  
  
  
  cannonposx = truckposx+15*(scalene/  6)
  cannonposy = truckposy+6*(scalene/6)
  
  
  
  rot = math.atan((cursory/scale-cannonposy+love.graphics.getHeight()*2*(scale-1))/(cursorx/scale-transx-cannonposx-transx))
  
  
  --love.graphics.arc( "fill", 400, 300, 100, rot+math.pi, rot)
  
  if level == 11 then else
  
  for k,v in pairs(lparams["building"]) do
    for k2,v2 in pairs(lparams["building"][k]) do
      drawwind(k,k2,1)
      if lparams["building"][k][k2] > 0 then
        if gamestart == true then 
          if lparams["building"][k][k2] == 2 then
            drawfire(k,k2,1,lparams["building"][k][k2])
          else
            drawsmolfire(k,k2,1,lparams["building"][k][k2])
          end
        end
      end
    end
  end
  
  drawfound(lparams["width"],1)
  drawroof(lparams["width"],lparams["width"],1)
  
  for k,v in pairs(nexbuil["building"]) do
    for k2,v2 in pairs(nexbuil["building"][k]) do
      drawwind(k,k2,2)
      if nexbuil["building"][k][k2] > 0 then
        drawfire(k,k2,2,nexbuil["building"][k][k2])
      end
    end
  end

  
  drawfound(lparams["width"],2)
  drawroof(lparams["width"],lparams["width"],2)
  
  end -- end level11 check
  --29,31
  
  love.graphics.draw(wheel,truckposx-transx+truck:getWidth()*scalene/6*(29.5/truck:getWidth()),truckposy+truck:getHeight()*scalene/6*(32.5/truck:getHeight()),wheelrot,scalene/6,scalene/6,wheel:getWidth()/2,wheel:getHeight()/2)
  love.graphics.draw(wheel,truckposx-transx+truck:getWidth()*scalene/6*(42.5/truck:getWidth()),truckposy+truck:getHeight()*scalene/6*(32.5/truck:getHeight()),wheelrot,scalene/6,scalene/6,wheel:getWidth()/2,wheel:getHeight()/2)
  love.graphics.draw(wheel,truckposx-transx+truck:getWidth()*scalene/6*(75.5/truck:getWidth()),truckposy+truck:getHeight()*scalene/6*(32.5/truck:getHeight()),wheelrot,scalene/6,scalene/6,wheel:getWidth()/2,wheel:getHeight()/2)
  
  love.graphics.draw(truck,truckposx-transx,truckposy+trucktran,0,1*scalene/6,1*scalene/6)
  
  
  
  --love.graphics.rectangle("fill",da["x"],da["y"],10,10)
  
  love.graphics.setColor(66, 119, 244)
  
  if joysticks[1]:isDown(1) and not istransition then
    love.graphics.setLineWidth(10)
    love.graphics.line(cannonposx-transx,cannonposy+trucktran,cursorx/scale-transx,cursory/scale+love.graphics.getHeight()*2*(scale-1))
 
    --[[for k,v in pairs(water) do
      
      ox=cursorx/scale-transx
      oy=cursory/scale+love.graphics.getHeight()*2*(scale-1)
      
      ox=ox-cannonposx-transx
      oy=oy-cannonposy
      
      nx=ox*math.cos(v["r"]) - oy*math.sin(v["r"])
      ny=oy*math.cos(v["r"]) + ox*math.sin(v["r"])
      
      love.graphics.line(cannonposx-transx,cannonposy+trucktran,nx+cannonposx-transx,ny+cannonposy)
      
    end]]
    
  end
  
  love.graphics.setColor(255,255,255)
  
  love.graphics.draw(cannon, cannonposx-transx, cannonposy+trucktran, rot, scalene/6, scalene/6,11,8.5)
  
  love.graphics.draw(cursor, cursorx/scale-transx,cursory/scale+love.graphics.getHeight()*2*(scale-1), 0, 4,4,cursor:getWidth()/2,cursor:getWidth()/2)
  love.graphics.origin()
  love.graphics.scale(1)
  
  if gamestart then
    if timer%60 < 10 then
      love.graphics.print("time left: "..math.floor(timer/60)..":0"..math.floor(timer%60).." fires:"..fires,0,0,0,3,3)
    else
      love.graphics.print("time left: "..math.floor(timer/60)..":"..math.floor(timer%60).." fires:"..fires.." "..level,0,0,0,3,3)
    end
  --elseif fires > 0 then
    --love.graphics.print("Game over, you put out "..fires.." fires in 5 minutes.\n Press any button to begin.",0,0,0,3,3)
  else
    --love.graphics.print("Press any button to begin.",0,0,0,3,3)
  end
  
  if gameover then
    love.graphics.draw(go,0,0,0,love.graphics.getWidth()/go:getWidth(),love.graphics.getHeight()/go:getHeight())
    love.graphics.setColor(0,0,0)
    love.graphics.print(fires.." ",love.graphics.getWidth()*(181/ts:getWidth()),love.graphics.getHeight()*(95/ts:getHeight()),0,3*4,3*4)
    --love.graphics.print(fires.." ",0,0,0,3,3)
    love.graphics.setColor(255,255,255)
  end
  if titlescreen then
    love.graphics.draw(ts,0,0,0,love.graphics.getWidth()/ts:getWidth(),love.graphics.getHeight()/ts:getHeight())
  end
  
end
function drawfound(w,level)
  for x=1,w do
    love.graphics.draw(foundmid,windposx(x)+road:getWidth()*scalene/3*10*(level-1),windposy(0),0,1*(scalene/10),1*(scalene/10))
  end
  love.graphics.draw(foundleft,windposx(0)+road:getWidth()*scalene/3*10*(level-1),windposy(0),0,1*(scalene/10),1*(scalene/10))
  love.graphics.draw(foundright,windposx(w+1)+road:getWidth()*scalene/3*10*(level-1),windposy(0),0,1*(scalene/10),1*(scalene/10))
end
function drawroof(w,h,level)
  for x=1,w do
    love.graphics.draw(roofmid,windposx(x)+road:getWidth()*scalene/3*10*(level-1),windposy(h+1),0,1*(scalene/10),1*(scalene/10))
  end
  love.graphics.draw(roofleft,windposx(0)+road:getWidth()*scalene/3*10*(level-1),windposy(h+1),0,1*(scalene/10),1*(scalene/10))
  love.graphics.draw(roofright,windposx(w+1)+road:getWidth()*scalene/3*10*(level-1),windposy(h+1),0,1*(scalene/10),1*(scalene/10))
end
function drawwind(c,r,level)
  love.graphics.draw(wind, windposx(c)+road:getWidth()*scalene/3*10*(level-1),windposy(r),0,1*(scalene/10),1*(scalene/10))
end

function drawsmolfire(c,r,level,t)
  
  love.graphics.draw(fire[rand],windposx(c+.5)+road:getWidth()*scalene/3*10*(level-1),windposy(r-.5),0,1*(scalene/lerp(20,10,t/2)),1*(scalene/lerp(20,10,t/2)),fire[rand]:getWidth()/2,fire[rand]:getHeight()/2)
  --love.graphics.print(tostring(t),windposx(c)+road:getWidth()*scalene/3*10*(level-1),windposy(r))
end

function drawfire(c,r,level,t)
  
  love.graphics.draw(fire[rand],windposx(c)+road:getWidth()*scalene/3*10*(level-1),windposy(r),0,1*(scalene/10),1*(scalene/10))
  --love.graphics.print(tostring(t),windposx(c)+road:getWidth()*scalene/3*10*(level-1),windposy(r))
end
function windposx(c)
  return ((c-1)*1*(scalene/10)*wind:getWidth())+love.graphics.getWidth()/1.5
end
function windposy(r)
  return love.graphics.getHeight()-road:getHeight()*(scalene/3)-wind:getHeight()*(scalene/10)*(r)-foundmid:getHeight()*(scalene/10)
end
function lerp(a,b,t) return a * (1-t) + b * t end

function tsupdate(dt)
  
end
function goupdate(dt)
  
end

function love.update(dt)
  if titlescreen then tsupdate(dt) end
  if gameover then goupdate(dt) end
  
  if istransition and gamestart then
    istransitionupdate(dt)
  elseif gamestart then
  
    isfireupdate(dt)
  else
    scale = love.graphics.getHeight()/(love.graphics.getHeight()-windposy(lparams["width"])+190)
    if (windposx(lparams["width"])+190)*scale > love.graphics.getWidth() then
      scale = love.graphics.getWidth()/(windposx(lparams["width"])+300)
    end
  end
end
trancounter = 0
trancounter2= 0
function istransitionupdate(dt)
  transky = transky-dt*500
  wheelrot=wheelrot+dt*10
  trancounter=trancounter+dt
  trancounter2 = trancounter2+dt
  transx=-(transfr+((transto-transfr)*(trancounter/10)))
  oldscale=scale
  scale=lerp(oldscale,newscale,trancounter/10)
  trucksound:setPitch(1.09)
  if trancounter2 > 5.5 then
    level=level+1
    lparams=bu[level]
    nexbuil=bu[level]
    trancounter2 = -4.5
  end
  if trancounter > 10 then
    trucksound:setPitch(1)
    transx=0
    istransition = false
    trancounter=0
    wheelrot=0
  end
end
function continue(level)
  
  
  
  
  newscale = love.graphics.getHeight()/(love.graphics.getHeight()-windposy(bu[level+1]["width"])+190)
  transto=road:getWidth()*scalene/3*10*1
  transfr=road:getWidth()*scalene/3*10*0
  
  istransition=true
  
end
recursion=0
function rfire() 
  love.math.setRandomSeed(os.time()+2+recursion)
  ra=love.math.random(1,lparams["width"])
  love.math.setRandomSeed(os.time()+4+recursion)
  ra2=love.math.random(1,lparams["width"])
  if lparams["building"][ra][ra2] == 5 then
    recursion = recursion + 1
    if recursion > 10 then
      return
    end
    rfire()
  end
  lparams["building"][ra][ra2] = 2
end
trucksound:setLooping(true)
trucksound:setVolume(0.15)

function isfireupdate(dt)
  
  timer = timer - dt
  
  if timer < 0 then
    level=1
    gotran=0
    tstran=0
    music:stop()
    trucksound:stop()
    gameover=true
    gamestart=false
  end
  
  if joysticks[1]:getAxis(1) > 0.08 or joysticks[1]:getAxis(2) > 0.08 or joysticks[1]:getAxis(1) < -0.08 or joysticks[1]:getAxis(2) < -0.08 then
    
    cursorx=cursorx+joysticks[1]:getAxis(1)*500*dt
    cursory=cursory+joysticks[1]:getAxis(2)*500*dt
    
  end
  


  if cursorx < 150 then
    cursorx = 150
  end
  
  nofire=true
  for k,v in pairs(lparams["building"]) do
    for k2,v2 in pairs(lparams["building"][k]) do
      if lparams["building"][k][k2] > 0 then
        nofire=false
      end
    end
  end
  if nofire then
    continue(level)
  end
  
  fcounter = fcounter + dt
  if fcounter > 15 - level then
    recursion=0
    rfire()
    
    fcounter = 0
  end
  
  love.mouse.setVisible(false)
  
  --scale = (windposy(lparams["width"])+94+love.graphics.getHeight()*.9)/love.graphics.getHeight()
  scale = love.graphics.getHeight()/(love.graphics.getHeight()-windposy(lparams["width"])+190)
  if (windposx(lparams["width"])+190)*scale > love.graphics.getWidth() then
    scale = love.graphics.getWidth()/(windposx(lparams["width"])+300)
  end
  love.window.setTitle(level.." "..love.timer.getFPS().." DEBUG: XINPUT1 CONTROLLER Axis1:"..joysticks[1]:getAxis(1).." Axis2:"..joysticks[1]:getAxis(2))
  
  tcounter=tcounter+dt
  
  truckcounter = truckcounter + dt
  
  if truckcounter > .8 then
    truckcounter = truckcounter -.8
    trucktran = trucktran * -1
  end
  
  if tcounter > .2 then
    tcounter = tcounter-.2
    tnumber = tnumber + 1
    
    love.math.setRandomSeed(tnumber)
    
    if rand == love.math.random(1,4) then
      tnumber = love.math.random(1,100)
    end
    
    
    love.math.setRandomSeed(tnumber)
    
    rand = love.math.random(1,4)
    
  end
  
  --raycast function
  if level==11 then else
  World = love.physics.newWorld()
  
  
  for k,v in pairs(lparams["building"]) do
    for k2,v2 in pairs(lparams["building"][k]) do
      if lparams["building"][k][k2] > 0 then
        Box = {}
        Box.Body = love.physics.newBody(World, windposx(k)+63, windposy(k2)+64, "static")
        Box.Shape = love.physics.newRectangleShape(63*(scalene/10), 64*(scalene/10))
        Box.Fixture = love.physics.newFixture(Box.Body, Box.Shape)
      end
    end
  end

  updatewater(dt)
  
  World:update(dt)
  if joysticks[1]:isDown(1) then
    World:rayCast(cannonposx,cannonposy+trucktran,cursorx/scale+31,cursory/scale+love.graphics.getHeight()*2*(scale-1),raycallback)
  end
  --World:rayCast(cannonposx,cannonposy+trucktran,love.mouse.getX(),love.mouse.getY(),raycallback)
  
  if didfixhit then
    
    if lparams["building"][fixhit["c"]][fixhit["r"]] > 0 then
    
      lparams["building"][fixhit["c"]][fixhit["r"]]= lparams["building"][fixhit["c"]][fixhit["r"]] - dt
      --lparams["building"][fixhit["c"]][fixhit["r"]]=0
      if lparams["building"][fixhit["c"]][fixhit["r"]] < 0 then
        fires = fires + 1
        
      end
      
      if lparams["building"][fixhit["c"]][fixhit["r"]] < 0 then
        lparams["building"][fixhit["c"]][fixhit["r"]] = 0
        print("fireded")
      end
      
    end
    
    didfixhit=false
  end
  end-- end level0 check
end

function raycallback(fix,x,y)
  --love.window.setTitle(x.." "..y)
  da["x"] = x
  da["y"] = y
  didfixhit=true
  
  for k,v in pairs(lparams["building"]) do
    for k2,v2 in pairs(lparams["building"][k]) do
      if lparams["building"][k][k2] > 0 then
        Box = {}
        Box.Body = love.physics.newBody(World, windposx(k)+63, windposy(k2)+64, "static")
        Box.Shape = love.physics.newRectangleShape(63*(scalene/10), 64*(scalene/10))
        Box.Fixture = love.physics.newFixture(Box.Body, Box.Shape)
        if Box.Fixture:testPoint(x,y) then
          fixhit["c"]=k
          fixhit["r"]=k2
          --lparams["building"][k][k2] = false
        end
      end
    end
  end
  
  
  return -1
end
function love.gamepadpressed( joystick, button )
  if gamestart==false and titlescreen==true then
    
    fires=0
    
    gamestart=true
    titlescreen=false
    music:seek(0)
    music:play()
    trucksound:play()
  elseif gamestart == false and gameover==true then
    gameover=false
    titlescreen=true
     timer=75
    
    bu[1]=genbuilding(1)
    bu[2]=genbuilding(2)
    bu[3]=genbuilding(3)
    bu[4]=genbuilding(4)
    bu[5]=genbuilding(5)
    bu[6]=genbuilding(6)
    bu[7]=genbuilding(7)
    bu[8]=genbuilding(8)
    bu[9]=genbuilding(9)
    bu[10]=genbuilding(10)
    bu[11]=genbuilding(11)
    lparams=bu[1]
    nexbuil=bu[2]
  end
end