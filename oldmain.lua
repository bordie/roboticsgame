--[[

Opening of main thread; code execution begins here.

Authored for TCS Robotics team by Brodie Alexander.

]]--

love.window.setMode(1920/1.5,1080/1.5)

--Texture generation

function rectgen(w,h,r,g,b)
  canvas = love.graphics.newCanvas(w,h)
  canvas:setFilter("nearest")
  love.graphics.setCanvas(canvas)
  love.graphics.setColor(r,g,b)
  love.graphics.rectangle("fill",0,0,w,h)
  love.graphics.setCanvas()
  return canvas
end

function tlen(tab)
  local i = 0
  local n = 0
  for k,v in pairs(tab) do
    i=i+1
    n=n+v
  end
  return i,n
end



function buildmake(bp)
  
  love.graphics.setColor(255,0,0)
  love.graphics.rectangle("fill",0,0,512,256)
  
  love.graphics.setColor(255,255,255)
  
  for k1,v1 in pairs(bp["wincol"]) do
    for k2,v2 in pairs(bp["winrow"]) do
      local i,n = tlen(bp["wincol"])
      local k,l = tlen(bp["winrow"])
      local winlen = ((((512-bp["wingap"])/n)-(bp["wingap"])*2))
      local winhei = ((((256-bp["wingap"])/l)-(bp["wingap"])*2))

      sx=winlen*(k1-1)+(bp["wingap"]*2*(k1-1))+bp["wingap"]*2
      sy=winhei*(k2-1)+(bp["wingap"]*2*(k2-1))+bp["wingap"]*2
      love.graphics.rectangle("fill",sx,sy,winlen*v1,winhei*v2)
      
    end
  end
  
end

brik = rectgen(128,16,237, 234, 175)

bp={}
bp["wincol"] = {1,2,1}
bp["winrow"] = {1,1,1}
bp["wingap"] = 13

canvas = love.graphics.newCanvas(w,h)
love.graphics.setCanvas(canvas)
building = buildmake(bp)
love.graphics.setCanvas()

function love.draw()
  love.graphics.draw(canvas, 22, 22)
end

function love.update(dt)
  
end